import { Component, OnInit } from '@angular/core';
import { BikeService } from '../bike.service';
import { IBike } from '../bike.model';

@Component({
  selector: 'app-bike-list',
  templateUrl: './bike-list.component.html',
  styleUrls: ['./bike-list.component.css']
})
export class BikeListComponent implements OnInit {

  bikesList: IBike[];


  constructor(private bikeService: BikeService) { }

  ngOnInit() {
    this.bikeService.query()
    .subscribe(res => {
      console.log('Get Data ', res);
      this.bikesList = res;
    }, error => {
      console.error("Error ", error);
    });
  }

}
