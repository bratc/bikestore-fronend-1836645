import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BikeService } from '../bike.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-bike-create',
  templateUrl: './bike-create.component.html',
  styleUrls: ['./bike-create.component.css']
})
export class BikeCreateComponent implements OnInit {

  formBikeCreate: FormGroup;

  constructor(
    private formBuilder: FormBuilder, 
    private bikeService: BikeService,
    private router: Router
    ) {

    this.formBikeCreate = this.formBuilder.group({
      model: ['', Validators.required],
      price: [''],
      serial: ['']
    });

   }

  ngOnInit() {
  }

  save(): void {
      console.warn('Datos',this.formBikeCreate.value);
      this.bikeService.saveBike(this.formBikeCreate.value)
      .subscribe(res => {
          console.warn('Save Ok ',res);
          this.router.navigate(['./bikes/bike-lists']);
      },error => {
        console.warn('Error ',error);
      });
  }

}
